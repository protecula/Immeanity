extern crate uuid;
extern crate regex;
extern crate rusqlite;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate socks;

mod incoming_requests;
mod onion_manager;
mod persistence;
mod command_line_interface;
mod outgoing_requests;
mod messages;

use onion_manager::Onion;
use incoming_requests::IncomingRequests;
use persistence::Persistence;

fn main() {
    let mut database = Persistence::new();
    database.start();
    let mut onion = Onion::new();
    match database.get_private_key() {
        Some(private_key) => {
            onion.set_private_key(private_key);
        }
        None => {}
    }
    match onion.start() {
        Ok(()) => {}
        Err(onion_error) => {
            command_line_interface::onion_failure(onion_error);
            return;
        }
    }

    database.set_private_key(&onion.private_key);

    let incoming_requests = IncomingRequests::new(&onion);
    incoming_requests.start();

    match command_line_interface::network_initialization_dialog() {
        command_line_interface::NetworkInitializationDialogResult::CreateANetwork => {
            command_line_interface::will_display_sent_messages();
        }
        command_line_interface::NetworkInitializationDialogResult::JoinAnExistingNetwork => {
            let domain = command_line_interface::join_network();
            if let Some(onion_address) = &onion.onion_address {
                outgoing_requests::send_message(&onion, &domain, &messages::Message::JoinRequest {
                    onion_address: onion_address.to_string()
                });
            }
        }
    }

    loop {}

    onion.kill();
}
