#[derive(Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Message {
    JoinRequest { onion_address: String }
}
