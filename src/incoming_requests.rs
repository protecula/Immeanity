use std::thread;
use std::net::TcpListener;
use std::io::{BufReader, BufRead, Read, Write};

use serde_json;

use command_line_interface;
use messages::Message;
use onion_manager::Onion;

pub struct IncomingRequests {
    incoming_port: u16
}

impl IncomingRequests {
    pub fn new(onion: &Onion) -> IncomingRequests {
        IncomingRequests {
            incoming_port: onion.server_port
        }
    }
    pub fn start(self) {
        thread::spawn(move || {
            let listener = TcpListener::bind(format!("127.0.0.1:{}", self.incoming_port)).unwrap();
            for stream in listener.incoming() {
                let mut stream = stream.unwrap();
                let buf_reader = BufReader::new(stream);
                for line in buf_reader.lines() {
                    let line = line.unwrap();
                    let message: Message = serde_json::from_str(&line).unwrap();

                    match message {
                        Message::JoinRequest { onion_address } => {
                            println!("Someone asked to join your circle of friends: {}", onion_address);
                        }
                    }

                    break;
                }
            }
        });
    }
}
