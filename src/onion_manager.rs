use uuid::Uuid;

use regex::Regex;

use std::process::{Command, Stdio, Child};
use std::io::{BufReader, BufRead, Read, Write};
use std::net::TcpStream;
use std::result::Result;

use command_line_interface;

pub struct Onion {
    pub private_key: String,
    pub server_port: u16,
    control_port: u16,
    pub proxy_port: u16,
    pub onion_address: Option<String>,
    process: Option<Child>
}

pub enum OnionError {
    BootstrappingFailed,
    AlreadyRunning,
    UnknownError(String)
}

impl Default for Onion {
    fn default() -> Onion {
        Onion {
            private_key: "NEW:BEST".to_string(),
            server_port: 4571,
            control_port: 6452,
            proxy_port: 6451,
            onion_address: None,
            process: None
        }
    }
}

impl Onion {
    pub fn new() -> Onion {
        Onion::default()
    }
    pub fn set_private_key(&mut self, private_key: String) {
        let private_key_regex = Regex::new("[a-zA-Z0-9]*:[a-zA-Z0-9+/=]*").unwrap();
        if private_key_regex.is_match(&private_key) {
            self.private_key = private_key;
        } else {
            command_line_interface::reject_private_key();
        }
    }
    pub fn set_server_port(&mut self, server_port: u16) {
        self.server_port = server_port;
    }
    pub fn set_control_port(&mut self, control_port: u16) {
        self.control_port = control_port;
    }
    pub fn set_proxy_port(&mut self, proxy_port: u16) {
        self.proxy_port = proxy_port;
    }

    pub fn start(&mut self) -> Result<(), OnionError> {
        let password = Uuid::new_v4().hyphenated().to_string();
        let password_hashing_process = Command::new("tor")
            .args(&[
                "--hash-password", &password,
                "--quiet"
            ])
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();
        let mut password_hash_buf_reader = BufReader::new(password_hashing_process.stdout.unwrap());
        let mut password_hash = String::new();
        password_hash_buf_reader.read_to_string(&mut password_hash).unwrap();
        password_hash = password_hash.trim().to_string();
        let mut process = Command::new("tor")
            .args(&[
                "--SocksPort", &self.proxy_port.to_string(),
                "--ControlPort", &self.control_port.to_string(),
                "--HashedControlPassword", &password_hash,
                "--DataDirectory", "tor_data"
            ])
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .unwrap();

        if let Some(ref mut stdout) = process.stdout {
            let buf_reader = BufReader::new(stdout);
            for line in buf_reader.lines() {
                let string = line.unwrap();
                command_line_interface::bootstrap_progress(&string);

                if string.contains("Problem bootstrapping") {
                    return Err(OnionError::BootstrappingFailed);
                }
                if
                    string.contains("Is Tor already running?")
                    || string.contains("It looks like another Tor process is running")
                {
                    return Err(OnionError::AlreadyRunning);
                }
                if string.contains("[err]") {
                    return Err(OnionError::UnknownError(string));
                }
                if string.contains("Tor has successfully opened a circuit. Looks like client functionality is working.") {
                    command_line_interface::bootstrap_success();
                    break;
                }
            }
        }

        let mut stream = TcpStream::connect(format!("127.0.0.1:{}", self.control_port))
            .expect("Can't connect to control port");

        write!(stream, "AUTHENTICATE \"{}\"\r\n", password).unwrap();
        write!(stream, "SETEVENTS HS_DESC\r\n").unwrap();
        write!(stream, "ADD_ONION {} PORT=80,{} FLAGS=DETACH\r\n", self.private_key, self.server_port).unwrap();

        let mut stream_buffer = BufReader::new(stream);
        let mut upload_event_count = 0;
        let mut uploaded_event_count = 0;
        let service_id_regex = Regex::new("250-ServiceID=(.*)").unwrap();
        let private_key_regex = Regex::new("250-PrivateKey=(.*)").unwrap();
        let mut service_id = String::new();
        let mut private_key = String::new();

        loop {
            let mut line = String::new();
            if stream_buffer.read_line(&mut line).is_err() {
                break;
            }
            if !line.contains("250-PrivateKey") {
                command_line_interface::tor_control_response(&line);
            }
            if line.contains("UPLOADED") {
                uploaded_event_count += 1;
            } else if line.contains("UPLOAD") {
                upload_event_count += 1;
            } else if line.contains("250-ServiceID") {
                service_id = service_id_regex.captures(&line).unwrap().get(1).unwrap().as_str().to_string();
                command_line_interface::display_service_id(service_id.to_owned());
            } else if line.contains("250-PrivateKey") {
                private_key = private_key_regex.captures(&line).unwrap().get(1).unwrap().as_str().to_string();
            }
            if upload_event_count == 6 && uploaded_event_count == 6 {
                command_line_interface::onion_success();
                break;
            }
        }
        self.onion_address = Some(service_id);
        self.private_key = private_key;
        self.process = Some(process);
        Ok(())
    }
    pub fn kill(self) {
        self.process.unwrap().kill().unwrap();
    }
}
