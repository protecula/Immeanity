use std::io;

use onion_manager::OnionError;

fn read_line() -> String {
    let stdin = io::stdin();
    let input = &mut String::new();
    stdin.read_line(input).unwrap();
    input.trim().to_string()
}

pub fn onion_failure(error: OnionError) {
    match error {
        OnionError::BootstrappingFailed => {
            println!("The bootstrapping process failed *miserably*.");
        }
        OnionError::AlreadyRunning => {
            println!("One instance of this application is already running.");
        }
        OnionError::UnknownError(error) => {
            println!("A mysterious error happened!");
            println!("{}", error);
        }
    }
}

pub fn onion_success() {
    println!("Successfully created a tunnel to the Internet!");
}

pub fn display_service_id(service_id: String) {
    println!("Your onion service ID is: {}", service_id);
}

pub fn reject_private_key() {
    println!("Supplied private key doesn't look like an actual Tor Onion Service private key, rejecting to avoid code injection.");
}

pub fn bootstrap_progress(progress: &String) {
    println!("{}", progress);
}

pub fn bootstrap_success() {
    println!("Finished bootstrapping! Proceeding to the next step.");
}

pub fn tor_control_response(response: &String) {
    println!("{}", response);
}

pub enum NetworkInitializationDialogResult {
    CreateANetwork,
    JoinAnExistingNetwork
}

pub fn network_initialization_dialog() -> NetworkInitializationDialogResult {
    loop {
        println!("Tor has initialized successfully. Thank you for making this far.");
        println!("In order for Immeanity to be of useful value, you must either create a network or join an existing one.");
        println!("(1) Create a network");
        println!("(2) Join an existing network");
        println!("Enter a number to proceed: ");

        let input = read_line();

        if input == "1" {
            return NetworkInitializationDialogResult::CreateANetwork;
        } else if input == "2" {
            return NetworkInitializationDialogResult::JoinAnExistingNetwork;
        }
    }
}

pub fn will_display_sent_messages() {
    println!("OK. Immeanity will print messages sent to you. Onions are the best!");
}

pub fn join_network() -> String {
    println!("Input an onion address to bootstrap: ");
    read_line()
}
