use std::net::SocketAddr;
use std::io::{BufReader, BufRead, Read, Write};

use socks::{Socks5Stream, TargetAddr};
use uuid::Uuid;
use serde_json::to_string;

use onion_manager::Onion;
use messages::Message;

pub fn send_message(onion: &Onion, domain: &String, message: &Message) {
    // These variables are needed to make use of Tor's stream isolation feature.
    let random_username = Uuid::new_v4().hyphenated().to_string();
    let random_password = Uuid::new_v4().hyphenated().to_string();

    let mut stream = Socks5Stream::connect_with_password(
        SocketAddr::from(([127, 0, 0, 1], onion.proxy_port)),
        TargetAddr::Domain(domain.to_string(), 80),
        &random_username,
        &random_password
    ).unwrap();

    write!(stream, "{}", to_string(message).unwrap());
}
