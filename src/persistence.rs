use rusqlite::Connection;

pub struct Persistence {
    database_file: String,
    connection: Connection
}

impl Default for Persistence {
    fn default() -> Persistence {
        Persistence {
            database_file: "database.db".to_string(),
            connection: Connection::open("database.db").unwrap()
        }
    }
}

impl Persistence {
    pub fn new() -> Persistence {
        Persistence::default()
    }
    pub fn set_database_file(&mut self, database_file: String) {
        self.database_file = database_file;
        self.connection = Connection::open(&self.database_file).unwrap();
    }
    pub fn start(&mut self) {
        self.connection.execute(
            "CREATE TABLE IF NOT EXISTS onion_service (
                private_key TEXT NOT NULL
            )"
        , &[]).unwrap();
    }
    pub fn set_private_key(&self, private_key: &String) {
        self.connection.execute("DELETE FROM onion_service", &[]).unwrap();
        self.connection.execute("INSERT INTO onion_service VALUES (?1)", &[private_key]).unwrap();
    }
    pub fn get_private_key(&self) -> Option<String> {
        let mut statement = self.connection
            .prepare("SELECT private_key FROM onion_service").unwrap();
        let private_key_iterator = statement.query_map(&[], |row| { row.get(0) }).unwrap();
        for private_key in private_key_iterator {
            return Some(private_key.unwrap());
        }
        None
    }
}
